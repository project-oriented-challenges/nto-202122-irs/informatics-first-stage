from itertools import permutations
def triangle(a,b,c):
    return a<b+c and b<a+c and c<a+b
x=map(int,input().split())
for t in list(permutations(x)):
    if triangle(t[0],t[1],t[2]) and triangle(t[0],t[3],t[4]):
        print(t[0])
        print(t[1],t[2])
        print(t[3],t[4]) 
        break
else:
    print(0)