n=int(input())
if n==1:
    print('Talking horse Julius was walking along the road and met 40 robbers.')
elif n<82 and n%2==0:
    print('- Hello, robber number '+str(n//2)+',- said talking horse Julius.')
elif n<82:
    print('- Hello, talking horse Julius,- said robber number '+str(n//2)+'.')
elif n<162 and n%2==0:
    print('- Hello, horse of robber number '+str((n-80)//2)+',- said talking horse Julius.')
elif n<162:
    print('- Hello, talking horse Julius,- said horse of robber number '+str((n-80)//2)+'.')
elif n<242 and n%2==0:
    print('- Goodbye, robber number '+str((n-160)//2)+',- said talking horse Julius.')
elif n<242:
    print('- Goodbye, talking horse Julius,- said robber number '+str((n-160)//2)+'.')
elif n<322 and n%2==0:
    print('- Goodbye, horse of robber number '+str((n-240)//2)+',- said talking horse Julius.')
elif n<322:
    print('- Goodbye, talking horse Julius,- said horse of robber number '+str((n-240)//2)+'.')
else:
    print('And talking horse Julius went on along the road.')