n,d,s=map(int,input().split())
m=1
t=0
for i in range(n):
    a,b=map(int,input().split())
    k=(a-t)//d
    t+=d*k
    m+=k
    if t+d<b:
        t=a
        m+=1
print(m+(s-t+d-1)//d)