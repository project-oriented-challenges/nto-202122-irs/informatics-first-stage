unique=set()
other=''
for x in input().replace(' ','').lower():
    if x in unique:
        other+=x
    else:
        unique.add(x)
print(other)
print(''.join(sorted(list(unique))))