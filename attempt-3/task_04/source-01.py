n=int(input())
a=list(map(int,input().split()))
b=list(map(int,input().split()))
h=max([a[i]+b[i] for i in range(n)])
s=[['.']*n for i in range(h)]
for i in range(n):
    for j in range(a[i]):
        s[j][i]='#'
    for j in range(a[i],a[i]+b[i]):
        s[j][i]='*'
for x in reversed(s):
    print(''.join(x))