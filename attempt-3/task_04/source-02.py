n=int(input())
a=list(map(int,input().split()))
b=list(map(int,input().split()))
h=max([a[i]+b[i] for i in range(n)])
for j in range(h):
    for i in range(n):
        if h-j <= a[i]:
            print('#',end='')
        elif h-j <= a[i]+b[i]:
            print('*',end='')
        else:
            print('.',end='')
    print()