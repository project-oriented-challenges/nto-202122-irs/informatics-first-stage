n,m=map(int,input().split())
ans=2**m
wset=set()
for x in input().split():
    if len(x)>m:
        wset.add(x[:m])
    else:
        ans-=2**(m-len(x))
print(ans-len(wset))