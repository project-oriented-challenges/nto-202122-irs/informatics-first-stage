n = int(input())
h = list(map(int,input().split()))
a1,a2=0,0
for i in range(1,n):
    if h[i]>h[i-1]:
        if h[i-1]>=2000:
            a2+=h[i]-h[i-1]
        elif h[i]<=2000:
            a1+=h[i]-h[i-1]
        else:
            a1+=2000-h[i-1]
            a2+=h[i]-2000
print(a1,a2)